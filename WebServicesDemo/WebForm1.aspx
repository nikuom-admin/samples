﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebServicesDemo.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function GetStudentByID() {
            var id = document.getElementById("txtID").value;
            WebServicesDemo.StudentService.GetStudentById(id, GetStudentByIDSuccessCallback, GetStudentByIDFailedCallback)
        }

        function GetStudentByIDSuccessCallback(result) {
            document.getElementById("txtName").value = result["Name"];
            document.getElementById("txtGender").value = result["Gender"];
            document.getElementById("txtTotalMarks").value = result["TotalMarks"];

        }

        function GetStudentByIDFailedCallback(error)
        {
            alert(error.get_message());
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="~/StudentService.asmx" />
            </Services>
        </asp:ScriptManager>
        <table style="font-family: Arial; border: 1px solid black;">
            <tr>
                <td>ID</td>
                <td>
                    <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
                    <input id="Button1" type="button" value="Get Student" onclick="GetStudentByID()" />
                </td>
            </tr>
            <tr>
                <td>Name</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    <asp:TextBox ID="txtGender" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Total Marks</td>
                <td>
                    <asp:TextBox ID="txtTotalMarks" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </form>
</body>
</html>
