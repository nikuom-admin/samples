﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebServicesDemo
{
    /// <summary>
    /// Summary description for CalculatorWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CalculatorWebService : System.Web.Services.WebService
    {
        [WebMethod(EnableSession=true, Description="This method adds 2 numbers.", CacheDuration=20)]
        public int Add(int firstNumber, int secondNumber)
        {
            List<string> calculations;

            if (Session["CALCULATIOMS"] == null)
            {
                calculations = new List<string>();
            }
            else
            {
                calculations = (List<string>)Session["CALCULATIOMS"];
            }

            string strRcentCalculation = string.Format("{0} + {1} = {2}", firstNumber, secondNumber, firstNumber + secondNumber);
            calculations.Add(strRcentCalculation);

            Session["CALCULATIOMS"] = calculations;

            return firstNumber + secondNumber;
        }

        [WebMethod(EnableSession=true)]
        public List<string> GetCalculations()
        {
            
            if (Session["CALCULATIOMS"] == null)
            {
                List<string> calculations = new List<string>();
                calculations.Add("You have not performed any calculations.");
                return calculations;
            }
            else
            {
               return (List<string>)Session["CALCULATIOMS"];
            }
        }
    }
}
