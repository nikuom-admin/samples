﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;


namespace CalculatorTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void DivideWithZero()
        {
            Assert.AreEqual(0, Program.Divide(1, 0));
        }

        [TestMethod]
        public void DivideWithZeroDivident()
        {
            Assert.AreEqual(0, Program.Divide(0, 1.1));
        }

        [TestMethod]
        public void DivideIntegers()
        {
            Assert.AreEqual(0.125, Program.Divide(1, 8));
        }

        [TestMethod]
        public void AddIntegers()
        {
            Assert.AreEqual(9, Program.Add(1, 8));
        }
    }

   
}
