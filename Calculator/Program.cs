﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            double num1 = 0;
            double num2 = 0;
            string operand = string.Empty;
            double answer;
            bool validFirstNumber = false;
            bool validSecondNumber = false;
            bool validOperand = false;

            do
            {
                #region mainCall
                do
                {
                    Console.WriteLine("Please enter 1'st number:");
                    validFirstNumber = ValidateNumber(Console.ReadLine(), ref num1);
                    if (!validFirstNumber)
                    {
                        Console.WriteLine("Number is invalid");
                    }
                } while (!validFirstNumber);


                do
                {
                    Console.WriteLine("Please choose operand (+, -, *, /):");
                    validOperand = ValidateOperand(Console.ReadLine(), ref operand);
                    if (!validOperand)
                    {
                        Console.WriteLine("Oprand is invalid");
                    }
                } while (!validOperand);

                do
                {
                    Console.WriteLine("Please enter 2'nd number:");
                    validSecondNumber = ValidateNumber(Console.ReadLine(), ref num2);
                    if (!validSecondNumber)
                    {
                        Console.WriteLine("Number is invalid");
                    }
                } while (!validSecondNumber);



                switch (operand)
                {
                    case "+":
                        answer = Add(num1, num2);
                        break;
                    case "-":
                        answer = Substract(num1, num2);
                        break;
                    case "*":
                        answer = Multiply(num1, num2);
                        break;
                    case "/":
                        answer = Divide(num1, num2);
                        break;
                    default:
                        answer = 0;
                        break;
                }

                showAnswer(num1, num2, operand, answer);
                //do you want to continue or escape
                #endregion
                Console.WriteLine("Press ESC to stop or Press any key to continue");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }
        private static void showAnswer(double num1, double num2, string operand, double answer)
        {
            if (!(operand == "/" && num2 == 0))
            {
                Console.WriteLine("Your answer is:\n" + num1.ToString() + " " + operand + " " + num2.ToString() + " = " + answer.ToString());
            }
        }

        private static bool ValidateNumber(string input, ref double number)
        {
            double num = 0;
            bool isValid = Double.TryParse(input, out num); //syste,.convert.todouble
            number = num;
            return isValid;
        }

        private static bool ValidateOperand(string input, ref string operand)
        {
            string[] operandArray = { "+", "-", "*", "/" };

            bool isValid = false;

            for (int i = 0; i < operandArray.Length; i++)
            {
                if (operandArray[i] == input.Trim())
                {
                    isValid = true;
                    operand = input.Trim();
                    break;
                }
            }

            return isValid;
        }

        public static double Add(double firstNumber, double secondNumber)
        {
            double answer = 0;
            try
            {
                // Overflow exception can occur.
                answer = firstNumber + secondNumber;
            }
            catch (System.OverflowException)
            {
                // The following line displays information about the error.
                Console.WriteLine("Error occured.");
            }
            return answer;
        }

        public static double Substract(double firstNumber, double secondNumber)
        {
            double answer = 0;
            try
            {
                // Overflow exception can occur.
                answer = firstNumber - secondNumber;
            }
            catch (System.OverflowException)
            {
                // The following line displays information about the error.
                Console.WriteLine("Error occured.");
            }
            return answer;
        }

        public static double Multiply(double firstNumber, double secondNumber)
        {
            double answer = 0;
            try
            {
                // Overflow exception can occur.
                answer = firstNumber * secondNumber;
            }
            catch (System.OverflowException)
            {
                // The following line displays information about the error.
                Console.WriteLine("Error occured.");
            }
            return answer;

        }

        public static double Divide(double firstNumber, double secondNumber)
        {
            double answer = 0;
            try
            {
                //performs division only if divisor is not zero
                if (secondNumber != 0)
                {
                    // Overflow exception can occur.
                    answer = firstNumber / secondNumber;
                }
                else
                {
                    Console.WriteLine("Division by zero is not valid. divisor should not be zero");
                }

            }
            catch (System.OverflowException e)
            {
                // The following line displays information about the error.
                Console.WriteLine("Error occured.");
            }
            return answer;
        }
    }
}
