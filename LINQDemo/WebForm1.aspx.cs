﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LINQDemo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SampleDataContext dataContext = new SampleDataContext();
            //GridView1.DataSource = from employee in dataContext.Employees
            //                       //where employee.Gender == "Male"
            //                       select employee;

            int[] numbers = { 1,2,3,4,5,6,7,8,9,10};
            //GridView1.DataSource = from number in numbers
            //                       where number % 2 == 0
            //                       select number;
            IEnumerable<int> queryResult = numbers.Where(i => i % 2 == 0);
            GridView1.DataSource = queryResult;
            GridView1.DataBind();
        }
    }
}